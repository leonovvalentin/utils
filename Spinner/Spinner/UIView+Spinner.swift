//
//  UIView+Spinner.swift
//  Spinner
//
//  Created by Валентин Леонов on 08/03/15.
//  Copyright (c) 2015 Noveo. All rights reserved.
//

import UIKit

let spinnerViewTag = 923478
let spinnerViewAlpha: CGFloat = 0.7
let spinnerViewStartDuration = 0.25
let spinnerViewStopDuration = 0.25

extension UIView {
    
    func startSpinner() {
        self.startSpinner(nil)
    }
    
    func startSpinner(completion: (Bool -> Void)?) {
        
        if self is UIActivityIndicatorView {
            return
        }
        
        let addFullscreenSubview = {(superview: UIView, subview: UIView) -> Void in
            
            superview.addSubview(subview)
            
            subview.setTranslatesAutoresizingMaskIntoConstraints(false)
            let views = ["v": subview]
            superview.addConstraints(
                NSLayoutConstraint.constraintsWithVisualFormat("|-0-[v]-0-|",
                    options: NSLayoutFormatOptions(0),
                    metrics: nil,
                    views: views) +
                    NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[v]-0-|",
                        options: NSLayoutFormatOptions(0),
                        metrics: nil,
                        views: views)
            )
        }
        
        let existingSpinnerView = self.viewWithTag(spinnerViewTag)
        let spinnerView = existingSpinnerView != nil ?
            existingSpinnerView! : UIView(frame: self.bounds)
        
        if existingSpinnerView == nil {
            
            spinnerView.tag = spinnerViewTag
            spinnerView.backgroundColor = UIColor.blackColor()
            spinnerView.alpha = 0
            spinnerView.hidden = true
            
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
            spinner.startAnimating()
            
            addFullscreenSubview(spinnerView, spinner)
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            
            addFullscreenSubview(self, spinnerView)
            
            spinnerView.hidden = false
            UIView.animateWithDuration(spinnerViewStartDuration,
                delay: 0,
                options: .BeginFromCurrentState,
                animations: {
                    spinnerView.alpha = spinnerViewAlpha
                },
                completion:completion
            )
        }
    }
    
    func stopSpinner() {
        self.stopSpinner(nil)
    }
    
    func stopSpinner(completion:(Bool -> Void)?) {
        
        if let spinnerView = self.viewWithTag(spinnerViewTag) {
            
            if spinnerView.hidden {
                return
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                UIView.animateWithDuration(spinnerViewStopDuration,
                    delay: 0,
                    options: .BeginFromCurrentState,
                    animations: {
                        spinnerView.alpha = 0
                    },
                    completion: {finished in
                        spinnerView.hidden = true
                        completion?(finished)
                    }
                )
            }
        }
    }
}
