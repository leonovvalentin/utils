//
//  AppDelegate.swift
//  Spinner
//
//  Created by Валентин Леонов on 08/03/15.
//  Copyright (c) 2015 Noveo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
}
